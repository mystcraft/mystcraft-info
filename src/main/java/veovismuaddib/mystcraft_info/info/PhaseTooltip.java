package veovismuaddib.mystcraft_info.info;

import com.xcompwiz.mystcraft.api.symbol.IAgeSymbol;
import veovismuaddib.mystcraft_info.ModConfig;
import veovismuaddib.mystcraft_info.helpers.ModifierHelper;

import java.util.ArrayList;
import java.util.List;

public class PhaseTooltip extends SymbolTooltip.SymbolTooltipGroup {
    public PhaseTooltip() {
        super.setHeader("gui.mystcraft_info.phase.header");
        super.add(new PercentPhaseTooltip());
        super.add(new DegreePhaseTooltip());
    }

    @Override
    public List<String> getTooltip(IAgeSymbol symbol) {
        List<String> tooltip = super.getTooltip(symbol);

        if(!isVisible(symbol)) return tooltip;

        Number phase = ModifierHelper.getPhaseFromSymbol(symbol);
        if(phase == null) return tooltip;

        return tooltip;
    }


    private static class PercentPhaseTooltip extends SymbolTooltip {
        @Override
        public boolean isEnabled() {
            return ModConfig.tooltips.phase_percent.enabled;
        }

        @Override
        public boolean requiresShift() {
            return ModConfig.tooltips.phase_percent.shift_required;
        }

        @Override
        public boolean isRelevant(IAgeSymbol symbol) {
            Number phase = ModifierHelper.getPhaseFromSymbol(symbol);
            return phase != null;
        }

        public List<String> getTooltip(IAgeSymbol symbol) {
            List<String> tooltip = new ArrayList<>();
            Number phase = ModifierHelper.getPhaseFromSymbol(symbol);

            if(!isVisible(symbol)) return tooltip;
            if(phase == null) return tooltip;

            float percentagePhase = (phase.floatValue() / 360) % 1;

            tooltip.add(DEFAULT_FORMAT + String.format("%d%%", Math.round(percentagePhase * 100)));

            return tooltip;
        }
    }

    private static class DegreePhaseTooltip extends SymbolTooltip {
        @Override
        public boolean isEnabled() {
            return ModConfig.tooltips.phase_degrees.enabled;
        }

        @Override
        public boolean requiresShift() {
            return ModConfig.tooltips.phase_degrees.shift_required;
        }

        @Override
        public boolean isRelevant(IAgeSymbol symbol) {
            Number phase = ModifierHelper.getPhaseFromSymbol(symbol);
            return phase != null;
        }

        public List<String> getTooltip(IAgeSymbol symbol) {
            List<String> tooltip = new ArrayList<>();
            Number phase = ModifierHelper.getPhaseFromSymbol(symbol);

            if(!isVisible(symbol)) return tooltip;
            if(phase == null) return tooltip;

            tooltip.add(DEFAULT_FORMAT + String.format("%.1f°", phase.floatValue()));

            return tooltip;
        }
    }
}
