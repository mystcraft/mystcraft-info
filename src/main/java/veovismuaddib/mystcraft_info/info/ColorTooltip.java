package veovismuaddib.mystcraft_info.info;

import com.xcompwiz.mystcraft.api.symbol.IAgeSymbol;
import com.xcompwiz.mystcraft.api.util.Color;
import veovismuaddib.mystcraft_info.ModConfig;
import veovismuaddib.mystcraft_info.ModMain;
import veovismuaddib.mystcraft_info.helpers.ModifierHelper;

import java.util.ArrayList;
import java.util.List;

public class ColorTooltip extends SymbolTooltip.SymbolTooltipGroup {
    public ColorTooltip() {
        super.setHeader("gui.mystcraft_info.color.header");
        super.add(new PercentColorTooltip());
        super.add(new HexColorTooltip());
    }

    @Override
    public List<String> getTooltip(IAgeSymbol symbol) {
        List<String> tooltip = super.getTooltip(symbol);

        if(!isVisible(symbol)) return tooltip;

        Color color = ModifierHelper.getColorFromSymbol(symbol);
        if(color == null) return tooltip;

        return tooltip;
    }

    private static class PercentColorTooltip extends SymbolTooltip {
        @Override
        public boolean isEnabled() {
            return ModConfig.tooltips.color_percent.enabled;
        }

        @Override
        public boolean requiresShift() {
            return ModConfig.tooltips.color_percent.shift_required;
        }

        @Override
        public boolean isRelevant(IAgeSymbol symbol) {
            Color color = ModifierHelper.getColorFromSymbol(symbol);
            return color != null;
        }

        public List<String> getTooltip(IAgeSymbol symbol) {
            List<String> tooltip = new ArrayList<>();
            Color color = ModifierHelper.getColorFromSymbol(symbol);

            if(!isVisible(symbol)) return tooltip;
            if(color == null) return tooltip;

            tooltip.add(DEFAULT_FORMAT + ModMain.proxy.localize(
                "gui.mystcraft_info.color.percent",
                Math.round(color.r * 100),
                Math.round(color.g * 100),
                Math.round(color.b * 100)
            ));

            return tooltip;
        }
    }

    private static class HexColorTooltip extends SymbolTooltip {
        @Override
        public boolean isEnabled() {
            return ModConfig.tooltips.color_hex.enabled;
        }

        @Override
        public boolean requiresShift() {
            return ModConfig.tooltips.color_hex.shift_required;
        }

        @Override
        public boolean isRelevant(IAgeSymbol symbol) {
            Color color = ModifierHelper.getColorFromSymbol(symbol);
            return color != null;
        }

        public List<String> getTooltip(IAgeSymbol symbol) {
            List<String> tooltip = new ArrayList<>();
            Color color = ModifierHelper.getColorFromSymbol(symbol);

            if(!isVisible(symbol)) return tooltip;
            if(color == null) return tooltip;

            tooltip.add(DEFAULT_FORMAT + String.format("#%06X", color.asInt()));

            return tooltip;
        }
    }
}
