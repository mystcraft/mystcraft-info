package veovismuaddib.mystcraft_info;

import net.minecraftforge.common.config.Config;
import veovismuaddib.mystcraft_info.info.TooltipConfig;

@Config(modid = ModMain.MODID, name = "mystcraft/mystcraft_info", category = "")
public class ModConfig {
    public static TooltipConfig tooltips = new TooltipConfig();
    public static PoemConfig poems = new PoemConfig();

    public static class PoemConfig {
        @Config.Comment("Upon encountering an unknown poem word, replace it with '???'")
        public boolean replace_with_question_marks = false;

        @Config.Comment("Upon encountering an unknown poem word, replace it with the name of the symbol")
        public boolean replace_with_name = true;
    }
}
