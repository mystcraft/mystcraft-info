package veovismuaddib.mystcraft_info.helpers;

import java.util.HashMap;

import com.xcompwiz.mystcraft.api.symbol.BlockCategory;
import com.xcompwiz.mystcraft.api.symbol.BlockDescriptor;
import com.xcompwiz.mystcraft.api.symbol.IAgeSymbol;
import com.xcompwiz.mystcraft.api.symbol.ModifierUtils;
import com.xcompwiz.mystcraft.api.util.Color;
import com.xcompwiz.mystcraft.api.world.logic.Modifier;

import net.minecraft.block.state.IBlockState;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.biome.Biome;
import veovismuaddib.mystcraft_info.ModMain;
import veovismuaddib.mystcraft_info.mocks.FakeAgeDirector;

public class ModifierHelper {
    private static final HashMap<ResourceLocation, Color> colorCache = new HashMap<>();
    private static final HashMap<ResourceLocation, Number> lengthCache = new HashMap<>();
    private static final HashMap<ResourceLocation, Number> directionCache = new HashMap<>();
    private static final HashMap<ResourceLocation, Number> phaseCache = new HashMap<>();
    private static final HashMap<ResourceLocation, IBlockState> blockCache = new HashMap<>();
    private static final HashMap<ResourceLocation, Biome> biomeCache = new HashMap<>();

    public static Color getColorFromSymbol(IAgeSymbol symbol) {
        if(symbol == null) return null;

        ResourceLocation location = symbol.getRegistryName();

        if(colorCache.containsKey(location)) {
            return colorCache.get(location);
        }

        FakeAgeDirector ageDirector = new FakeAgeDirector();
        Color color = null;

        try {
            ageDirector.register(symbol);
            Modifier colorModifier = ageDirector.popModifier(ModifierUtils.COLOR);
            if(colorModifier != null) {
                color = colorModifier.asColor();
            }
        } catch(Exception e) {
            ModMain.logger.warn(e);
        }

        colorCache.put(location, color);
        return color;
    }

    public static Number getLengthFromSymbol(IAgeSymbol symbol) {
        if(symbol == null) return null;

        ResourceLocation location = symbol.getRegistryName();

        if(lengthCache.containsKey(location)) {
            return lengthCache.get(location);
        }

        FakeAgeDirector ageDirector = new FakeAgeDirector();
        Number length = null;

        try {
            ageDirector.register(symbol);
            Modifier lengthModifier = ageDirector.popModifier(ModifierUtils.FACTOR);
            if(lengthModifier != null) {
                length = lengthModifier.asNumber();
            }
        } catch(Exception e) {
            ModMain.logger.warn(e);
        }

        lengthCache.put(location, length);
        return length;
    }

    public static Number getDirectionFromSymbol(IAgeSymbol symbol) {
        if(symbol == null) return null;

        ResourceLocation location = symbol.getRegistryName();

        if(directionCache.containsKey(location)) {
            return directionCache.get(location);
        }

        FakeAgeDirector ageDirector = new FakeAgeDirector();
        Number direction = null;

        try {
            ageDirector.register(symbol);
            Modifier directionModifier = ageDirector.popModifier(ModifierUtils.ANGLE);
            if(directionModifier != null) {
                direction = directionModifier.asNumber();
            }
        } catch(Exception e) {
            ModMain.logger.warn(e);
        }

        directionCache.put(location, direction);
        return direction;
    }

    public static Number getPhaseFromSymbol(IAgeSymbol symbol) {
        if(symbol == null) return null;

        ResourceLocation location = symbol.getRegistryName();

        if(phaseCache.containsKey(location)) {
            return phaseCache.get(location);
        }

        FakeAgeDirector ageDirector = new FakeAgeDirector();
        Number phase = null;

        try {
            ageDirector.register(symbol);
            Modifier phaseModifier = ageDirector.popModifier(ModifierUtils.PHASE);
            if(phaseModifier != null) {
                phase = phaseModifier.asNumber();
            }
        } catch(Exception e) {
            ModMain.logger.warn(e);
        }

        phaseCache.put(location, phase);
        return phase;
    }

    public static IBlockState getBlockStateFromSymbol(IAgeSymbol symbol) {
        if(symbol == null) return null;

        ResourceLocation location = symbol.getRegistryName();

        if(blockCache.containsKey(location)) {
            return blockCache.get(location);
        }

        FakeAgeDirector ageDirector = new FakeAgeDirector();
        IBlockState state = null;

        try {
            ageDirector.register(symbol);
            BlockDescriptor descriptor = ModifierUtils.popBlockMatching(ageDirector, BlockCategory.ANY);
            if(descriptor != null) {
                state = descriptor.blockstate;
            }
        } catch(Exception e) {
            ModMain.logger.warn(e);
        }

        blockCache.put(location, state);
        return state;
    }

    public static Biome getBiomeFromSymbol(IAgeSymbol symbol) {
        if(symbol == null) return null;

        ResourceLocation location = symbol.getRegistryName();

        if(biomeCache.containsKey(location)) {
            return biomeCache.get(location);
        }

        FakeAgeDirector ageDirector = new FakeAgeDirector();
        Biome biome = null;

        try {
            ageDirector.register(symbol);
            biome = ModifierUtils.popBiome(ageDirector);
        } catch(Exception e) {
            ModMain.logger.warn(e);
        }

        biomeCache.put(location, biome);
        return biome;
    }
}
