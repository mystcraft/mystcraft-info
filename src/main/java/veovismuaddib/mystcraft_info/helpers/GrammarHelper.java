package veovismuaddib.mystcraft_info.helpers;

import com.xcompwiz.mystcraft.api.symbol.IAgeSymbol;
import net.minecraft.util.ResourceLocation;
import veovismuaddib.mystcraft_info.ModMain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class GrammarHelper {
    private static final HashMap<ResourceLocation, List<String>> grammarCache = new HashMap<>();
    private static final List<String> blacklistedRules = Stream.of(
        "featurelarges0",
        "featuremediums0",
        "featuresmalls0"
    ).collect(Collectors.toList());

    public static List<String> getGrammarFromSymbol(IAgeSymbol symbol) {
        if(symbol == null) return null;

        ResourceLocation location = symbol.getRegistryName();
        if(grammarCache.containsKey(location)) {
            return grammarCache.get(location);
        }

        List<String> grammarRules = new ArrayList<>();
        Collection<ResourceLocation> grammarRuleLocations = ModMain.proxy.grammarApi.getTokensProducingToken(symbol.getRegistryName());

        for(ResourceLocation rule: grammarRuleLocations) {
            if(!blacklistedRules.contains(rule.getResourcePath())) {
                grammarRules.add(getGrammarName(rule));
            }
        }

        grammarCache.put(location, grammarRules);
        return grammarRules;
    }

    private static String getGrammarName(ResourceLocation rule) {
        String[] name = {"grammar", rule.getResourceDomain(), rule.getResourcePath(), "name"};
        String unlocalizedName = String.join(".", name);
        return ModMain.proxy.localize(unlocalizedName);
    }
}
