package veovismuaddib.mystcraft_info.mocks;

import com.xcompwiz.mystcraft.api.symbol.IAgeSymbol;
import com.xcompwiz.mystcraft.api.util.ColorGradient;
import com.xcompwiz.mystcraft.api.world.AgeDirector;
import com.xcompwiz.mystcraft.api.world.logic.*;
import net.minecraft.world.biome.BiomeProvider;
import veovismuaddib.mystcraft_info.ModMain;

import java.util.*;

/**
 * Provides a no-logic AgeDirector that just registers interfaces and modifiers.
 */
public class FakeAgeDirector implements AgeDirector {
    public List<IBiomeController> biomeControllers = new ArrayList<>();
    public List<ITerrainGenerator> terrainGenerators = new ArrayList<>();
    public List<ILightingController> lightingControllers = new ArrayList<>();
    public List<IWeatherController> weatherControllers = new ArrayList<>();
    public List<ICelestial> celestials = new ArrayList<>();
    public List<ITerrainAlteration> terrainAlterations = new ArrayList<>();
    public List<IChunkProviderFinalization> chunkProviderFinalizations = new ArrayList<>();
    public List<IPopulate> populators = new ArrayList<>();
    public List<ITerrainFeatureLocator> terrainFeatureLocators = new ArrayList<>();
    public List<ISpawnModifier> spawnModifiers = new ArrayList<>();
    public List<IDynamicColorProvider> dynamicColorProviders = new ArrayList<>();
    public List<IStaticColorProvider> staticColorProviders = new ArrayList<>();
    public List<IEnvironmentalEffect> environmentalEffects = new ArrayList<>();

    public final HashMap<String, Deque<Modifier>> modifiers = new HashMap<>();

    public final List<IAgeSymbol> symbols = new ArrayList<>();

    public void register(IAgeSymbol symbol) {
        try {
            symbol.registerLogic(this, getSeed() + symbols.size());
            symbols.add(symbol);
        } catch(Exception e) {
            ModMain.logger.error(e);
        }
    }

    @Override
    public void registerInterface(IBiomeController biomeController) {
        biomeControllers.add(biomeController);
    }

    @Override
    public void registerInterface(ITerrainGenerator terrainGenerator) {
        terrainGenerators.add(terrainGenerator);
    }

    @Override
    public void registerInterface(ILightingController lightingController) {
        lightingControllers.add(lightingController);
    }

    @Override
    public void registerInterface(IWeatherController weatherController) {
        weatherControllers.add(weatherController);
    }

    @Override
    public void registerInterface(ICelestial celestial) {
        celestials.add(celestial);
    }

    @Override
    public void registerInterface(ITerrainAlteration terrainAlteration) {
        terrainAlterations.add(terrainAlteration);
    }

    @Override
    public void registerInterface(IChunkProviderFinalization chunkProviderFinalization) {
        chunkProviderFinalizations.add(chunkProviderFinalization);
    }

    @Override
    public void registerInterface(IPopulate populator) {
        populators.add(populator);
    }

    @Override
    public void registerInterface(ITerrainFeatureLocator terrainFeatureLocator) {
        terrainFeatureLocators.add(terrainFeatureLocator);
    }

    @Override
    public void registerInterface(ISpawnModifier spawnModifier) {
        spawnModifiers.add(spawnModifier);
    }

    @Override
    public void registerInterface(IDynamicColorProvider dynamicColorProvider, String s) {
        dynamicColorProviders.add(dynamicColorProvider);
    }

    @Override
    public void registerInterface(IStaticColorProvider staticColorProvider, String s) {
        staticColorProviders.add(staticColorProvider);
    }

    @Override
    public void registerInterface(IEnvironmentalEffect environmentalEffect) {
        environmentalEffects.add(environmentalEffect);
    }

    @Override
    public void setModifier(String name, Object modifier) {
        if(!(modifier instanceof Modifier)) {
            modifier = new Modifier(modifier);
        }
        setModifier(name, (Modifier)modifier);
    }

    @Override
    public void setModifier(String name, Modifier modifier) {
        Deque<Modifier> stack = modifiers.getOrDefault(name, new ArrayDeque<>());

        stack.push(modifier);
        modifiers.put(name, stack);
    }

    @Override
    public Modifier popModifier(String name) {
        try {
            return modifiers.get(name).pop();
        } catch(Exception e) {
            return new Modifier();
        }
    }

    @Override
    public void clearModifiers() {
        modifiers.clear();
    }

    @Override
    public long getTime() {
        return 0;
    }

    @Override
    public int getInstabilityScore() {
        return 0;
    }

    @Override
    public float getCloudHeight() {
        return 0;
    }

    @Override
    public double getHorizon() {
        return 0;
    }

    @Override
    public int getAverageGroundLevel() {
        return 0;
    }

    @Override
    public int getSeaLevel() {
        return 0;
    }

    @Override
    public long getSeed() {
        return 0;
    }

    @Override
    public BiomeProvider getBiomeProvider() {
        return null;
    }

    @Override
    public ColorGradient getSunriseSunsetColor() {
        return null;
    }

    @Override
    public void addInstability(int i) {}

    @Override
    public void setCloudHeight(float v) {}

    @Override
    public void setHorizon(double v) {}

    @Override
    public void setAverageGroundLevel(int i) {}

    @Override
    public void setSeaLevel(int i) {}

    @Override
    public void setDrawHorizon(boolean b) {}

    @Override
    public void setDrawVoid(boolean b) {}

    @Override
    public void setPvPEnabled(boolean b) {}
}
