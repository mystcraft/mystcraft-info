# 1.0.4.0

* Add Parent Mod tooltips to show what mod a given symbol was added by.  Contributed by Floydman.

# 1.0.3.0

* Add in-game config via Forge Mod Options menu.
* Removed confusing config options.
* Replace previous config file layout with a significantly simpler format.  To avoid confusion, please delete your previous config file and generate a new one.
* Localized shift required message.
* Enable color percent tooltips by default.
* Enable length percent tooltips by default.
* Display grammar rules for Lacking X Features symbols.
* Various bugfixes.

# 1.0.2.2

* Fix config options for similar types not being respected if at least one is enabled.  Caused by a refactoring in 1.0.1.0 that missed calling the parent method.  Resolved by making isEnabled check explicit.

# 1.0.2.1

* Add phase localization, which was missed in the original commit.

# 1.0.2.0

* Added Length tooltips, as a decimal or as a percentage of a Minecraft day. (Configurable, default false)
* Added Direction tooltips, as a percentage of a circle or as a value in degrees. (Configurable, default false)
* Added Phase tooltips, as a percentage of a circle or as a value in degrees. (Configurable, default false)

# 1.0.1.0

* Added shift toggle for all tooltip information.  Requires Mystcraft 0.13.6.0 or greater.
* Added symbol colors to tooltips.  Hex and RGB percent options available. (Configurable, default false)
* Split poem tooltips to separate lines to avoid going off the screen.
* Fixed crash on server side
* Internal refactoring
* Cache potentially expensive lookups by symbol name

# 1.0.0.0

* [Tooltip] Display Narayan poem components on Symbols (Configurable, default true)
* [Tooltip] Display grammar rules satisfied by a Symbol (Configurable, default true)
* [Config] Replace unknown words in a poem with "???" (default false)
* [Config] Replace unknown words with symbol name, disable if there are multiple unknown words per symbol (default true)
