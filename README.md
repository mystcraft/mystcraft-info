# Mystcraft Info

An small addon to [Mystcraft][] providing tooltips on symbol pages.  Will show the words in the Narayan poem as well as the grammar rules that the given symbol satisfies.

A tooltip with unknown words replaced with the symbol name:

![Example Tooltip 2][]

A tooltip in the Writing Desk:

![Writing Desk Tooltip][]

A tooltip in a Descriptive Book:

![Descriptive Book Tooltip][]

A tooltip with unknown words replaced with `???`:

![Example Tooltip][]



## Configuration

Edit the config file at `.minecraft/config/mystcraft/mystcraft_info.cfg` to tweak the mod to your liking.  Configuration options are only updated on startup.  See comments for details.

## Localization

Because grammar rules are not normally exposed to the player, they must be localized for this mod to properly display them.  See the [lang][] file for an example in English.

## Contributing

### Dependencies

Until I get maven situated, you will need to download the [Mystcraft][] API from CurseForge and place it in `./libs/`.

### Setup

```sh
./gradlew setupDecompWorkspace
```

### Build

```sh
./gradlew build
```

### Run

```sh
./gradlew runClient
./gradlew runServer
```

### Deploy

```sh
export CURSE_API_KEY="XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX"
export CURSE_RELEASE_TYPE="release|beta|alpha"
./gradlew curseforge
```

[Descriptive Book Tooltip]: ./src/main/resources/assets/mystcraft_info/screenshots/Descriptive%20Book%20Tooltip.png
[Example Tooltip 2]: ./src/main/resources/assets/mystcraft_info/screenshots/Tooltip%20Example%202.png
[Example Tooltip]: ./src/main/resources/assets/mystcraft_info/screenshots/Tooltip%20Example.png
[Writing Desk Tooltip]: ./src/main/resources/assets/mystcraft_info/screenshots/Writing%20Desk%20Tooltip.png

[lang]: ./src/main/resources/assets/mystcraft_info/lang/en_us.lang

[Mystcraft]: https://minecraft.curseforge.com/projects/mystcraft
